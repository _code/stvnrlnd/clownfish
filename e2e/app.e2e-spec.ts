import { ClownfishPage } from './app.po';

describe('clownfish App', () => {
  let page: ClownfishPage;

  beforeEach(() => {
    page = new ClownfishPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
