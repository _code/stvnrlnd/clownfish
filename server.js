'use strict';

// ------ Require the dependencies
var express = require('express');

// ------ Configure the application
var server = express();
var config = require('./server_resources/config');
var dictionary = require('./server_resources/dictionary.json');

// ------ Build the routes
server.get('/', function(req, res) {
    res.json({
        "message": "Hello, beautiful!",
        "dict": dictionary
    });
});

// ------ Serve the public
server.listen(config.port, function() {
    console.log('Running on port', config.port);
});